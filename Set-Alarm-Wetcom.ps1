# Variable
$wetcomAlarms = @(
	"Host battery status",
	"Host connection failure",
	"Host hardware fan status",
	"Host hardware power status",
	"Host hardware system board status",
	"Host hardware temperature status",
	"Host hardware voltage",
	"Insufficient vSphere HA failover resources",
	"Network connectivity lost",
	"Network uplink redundancy degraded",
	"Network uplink redundancy lost",
	"Unmanaged workload detected on SIOC-enabled datastore",
	"vSphere HA failover in progress",
	"Cannot connect to storage",
	"Certificate Status",
	"Errors occurred on the disk(s) of a vSAN host",
	"ESXi Host Certificate Status",
	"vSAN capacity utilization alarm 'What if the most consumed host fails'",
	"vSAN cluster alarm 'vSAN daemon liveness'",
	"vSAN network alarm 'Hosts with connectivity issues'",
	"vSAN network alarm 'vSAN cluster partition'",
	"vSAN online health alarm 'vSAN critical alert regarding a potential data inconsistency'",
	"vSAN physical disk alarm 'Component limit'",
	"vSAN physical disk alarm 'Congestion'"
)
# Conexi�n a VCenter
$vcenter=read-host "Por favor ingrese el nombre o IP del VCenter a configurar"
# connect-viserver $vcenter -alllinked
connect-viserver $vcenter

# Correo a Configurar
$proyecto = read-host "Ingrese el ID de proyecto (Ej: SWP)"
$token = read-host "Ingrese el Token"

foreach ($item in $wetcomAlarms) {
    $alarm = Get-AlarmDefinition $item
    $alarm | Get-AlarmAction | Where-Object {$_.ActionType -eq "ExecuteScript" -and $_.ScriptFilePath.IndexOf("/var/wetcom/create-jira-ticket.sh") -eq 0} | Remove-AlarmAction -Confirm:$false
    $alarm | New-AlarmAction -Script -ScriptPath "/var/wetcom/create-jira-ticket.sh $proyecto $token"
}

Write-Host "Listo."

