#!/bin/bash

PROJECT=$1
TOKEN=$2

SUBJECT="[${VMWARE_ALARM_NAME}] ${VMWARE_ALARM_EVENTDESCRIPTION}"
BODY="Target: ${VMWARE_ALARM_TARGET_NAME} \n Previous Status: ${VMWARE_ALARM_OLDSTATUS} \n New Status: ${VMWARE_ALARM_NEWSTATUS} \n\n Alarm Definition: \n${VMWARE_ALARM_DECLARINGSUMMARY} \n\n Description: \n${VMWARE_ALARM_EVENTDESCRIPTION}"

DATA='{ 
            "fields": { 
            "project": 
            {
                "key": "<project>" 
            }, 
            "summary": "<summary>", 
            "description": "<description>", 
            "issuetype": { 
                "name": "Alarma" 
            } 
        } 
        }'

DATA="${DATA/<summary>/$SUBJECT}"
DATA="${DATA/<description>/$BODY}"
DATA="${DATA/<project>/$PROJECT}"

curl -i \
-H "Accept: application/json" \
-H "Content-Type:application/json" \
-H "Authorization:Basic ${TOKEN}" \
-X POST --data "$DATA" https://wetcom.atlassian.net/rest/api/latest/issue/ >/dev/null 2>&1

