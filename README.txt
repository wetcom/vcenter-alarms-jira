Script para reportar alarmas en Jira Service Desk
=================================================

# In vCenter Appliance
mkdir /var/wetcom
cd /var/wetcom
wget http://gitserver.wetcom.net/dbacalov/vcenter-alarms-jira-sh/raw/master/create-jira-ticket.sh
chmod +x create-jira-ticket.sh

# In vSphere Client
Configure Alarm
In the Actions column, select Run a command from the drop-down menu
Set the script to:
/var/wetcom/create-jira-ticket.sh JiraProject Token

